# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.core.cache import caches
import erppeek




class OdooUser(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, blank=False, related_name='odoo_user')

    def __init__(self, *args, **kwargs):
        config = getattr(settings, "ODOO_HOST", False)
        super(OdooUser, self).__init__(*args, **kwargs)
        passwd = kwargs.get('password') or caches["odoo_auth"].get('%s_credentials' % self.user.username)
        self.odoo_client = erppeek.Client("%s:%d" % (config['HOST'], config['PORT']), db=config['DB'],
                                          user=self.user.username, password=passwd, verbose=False)
