import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()

setup(
    name='generic_odoo_django',
    version='0.1',
    packages=['app1'],
    description='app that connects to django and odoo app',
    long_description=README,
    author='jai',
    author_email='jai@anipr.in',
    url='https://bitbucket.org/jai_anipr/generic_odoo_django/',
    license='MIT',
    install_requires=[
        'Django>=1.8',
    ]
)